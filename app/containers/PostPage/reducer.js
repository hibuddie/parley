import { fromJS } from 'immutable';
import {
  LOAD_POST, LOAD_POST_SUCCESS, LOAD_POST_ERROR, POST_COMMENT_SUCCESS, POST_COMMENT_ERROR,
  POST_COMMENT, SAVE_ASKSTEEM_POSTS, SAVE_RELATIVE_POSTS, CHECK_EMBEDABLE } from './constants';
import {addCommentToPost} from 'utils/commen';

const initState = fromJS({
  loading: false,
  error: false,
  post: false,
  asksteemPosts: false,
  relativePosts: false,
  embedable: false,
});

function postReducer(state = initState, action) {
  switch (action.type) {
    case LOAD_POST:
      return state.set('loading', true)
        .set('post', false)
        .set('relativePosts', false)
        .set('embedable', false)
        .set('asksteemPosts', false)
        .set('error', false);
    case LOAD_POST_SUCCESS:
      return state.set('loading', false)
        .set('post', action.post);
    case LOAD_POST_ERROR:
      return state.set('loading', false)
        .set('error', action.error);
    case POST_COMMENT:
      return state.set('loading', true)
        .set('error', false);
    case POST_COMMENT_SUCCESS:
      const post = state.get('post');
      const newPost = addCommentToPost(post, action.comment);
      return state.set('loading', false)
        .set('post', newPost);
    case POST_COMMENT_ERROR:
      return state.set('loading', false)
        .set('error', action.error);
    case SAVE_ASKSTEEM_POSTS:
      return state.set('asksteemPosts', action.posts);
    case CHECK_EMBEDABLE:
      return state.set('embedable', action.payload);
    case SAVE_RELATIVE_POSTS:
      return state.set('relativePosts', action.posts);
    default:
      return state;
  }
}

export default postReducer;
