import { LOAD_POST, LOAD_POST_ERROR, LOAD_POST_SUCCESS,
    POST_COMMENT, POST_COMMENT_SUCCESS, POST_COMMENT_ERROR,
    SAVE_ASKSTEEM_POSTS, SAVE_RELATIVE_POSTS, CHECK_EMBEDABLE } from './constants';

export function loadPost(author, permlink) {
  return {
    type: LOAD_POST,
    author,
    permlink,
  };
}

export function postLoaded(post) {
  return {
    type: LOAD_POST_SUCCESS,
    post,
  };
}

export function postLoadingError(error) {
  return {
    type: LOAD_POST_ERROR,
    error,
  };
}

export function postComment(payload) {
  return {
    type: POST_COMMENT,
    payload,
  };
}


export function commentPosted(comment) {
  return {
    type: POST_COMMENT_SUCCESS,
    comment,
  };
}

export function commentPostingError(error) {
  return {
    type: POST_COMMENT_ERROR,
    error,
  };
}

export function saveAskSteemPosts(posts) {
    return {
      type: SAVE_ASKSTEEM_POSTS,
      posts,
    }
}

export function saveRelativePosts(posts) {
    return {
        type: SAVE_RELATIVE_POSTS,
        posts,
    }
}

export function checkEmbedable(payload) {
    return {
        type: CHECK_EMBEDABLE,
        payload,
    }
}
