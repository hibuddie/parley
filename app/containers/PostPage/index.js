import React from 'react';
import steem from 'steem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import {
  Col,
  Container,
  Row,
  Card } from 'reactstrap';
import { Link } from 'react-router-dom';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import Markdown from 'markdown-to-jsx';
import 'whatwg-fetch';
import { ScaleLoader } from 'react-spinners';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import Comments from 'components/Comments';
import Vote from 'components/Vote';
import Wrapper from 'containers/Wrapper/Loadable';
import reducer from './reducer';
import saga from './saga';
import { makeSelectPost, makeSelectLoading, makeSelectError, makeSelectAsksteemPosts, makeSelectRelativePosts, makeSelectEmbedable} from './selectors';
import {loadPost, postComment, saveAskSteemPosts, saveRelativePosts, checkEmbedable} from './actions';
import './style.css';
import { getCurrentUser} from "utils/user";
import PostItem from 'components/PostItem';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import {STEEMIT_API_URL} from "../App/constants";
import {removeParleyFromTag} from 'utils/commen';
import Iframe from 'react-iframe'

export class PostPage extends React.PureComponent {

  componentDidMount() {
    const { author, permlink } = this.props.match.params;
    this.props.loadPost(author, permlink);

    // Load Asksteem related posts.
    // const url = `https://api.asksteem.com/related?author=${author}&permlink=${permlink}`;
    // fetch(url)
    //     .then(response => response.json())
    //     .then(data => {
    //       if (!data.error) {
    //         if (data.results.length > 3)
    //             this.props.saveAsksteemPosts(data.results.slice(0, 3));
    //         else
    //             this.props.saveAsksteemPosts(data.results);
    //       }
    //     });
  }

  componentWillReceiveProps(nextProps) {
      const {post, relativePosts} = nextProps;

      // Load more trending posts in parent_permlink.
      steem.api.setOptions({ url: STEEMIT_API_URL });

      if (post && !relativePosts) {
          // Check Embedable
          const jsonMetadata = JSON.parse(post.json_metadata);
          if (jsonMetadata.app && jsonMetadata.app.name && jsonMetadata.app.name === 'parley') {
              const url = `https://api.parley.io/oembed?url=${jsonMetadata.appdata.parley.url}&format=json`;
              fetch(url, {mode: 'cors'})
                  .then(response => response.json())
                  .then(data => {
                      this.props.setEmbedable(data);
                  });
          }

          // Get relative posts.
          const tag = post.parent_permlink.includes('parley-') ? post.parent_permlink : `parley-${post.parent_permlink}`;
          let query = { limit: 3, tag };

          steem.api.getDiscussionsByCreated(query,
              ((error, response) => {
                  if (error) {
                      console.log(error);
                  } else {
                      const filtered = response.filter(r => r.id !== post.id);
                      this.props.saveRelativePosts(filtered);
                  }
              })
          );
      }
  }

  render() {
    const { post, loading, error, replyHandler, asksteemPosts, relativePosts, embedable } = this.props;

    if (post.author === '') {
      return (<NotFoundPage/>);
    }

    let content = null;
    let videoUrl = '';
    if (post) {
      const jsonMetadata = JSON.parse(post.json_metadata);
      let isParley = false;
      if (jsonMetadata.app && jsonMetadata.app.name && jsonMetadata.app.name === 'parley') {
          isParley = true;
          videoUrl = jsonMetadata.appdata.parley.url;
      }

      content = <div>
          <Row>
                <PostItem post={post} key={post.id}/>
          </Row>
          <Row>
            <Col sm={{ size: 12 }}>
                {!isParley &&
                    <Markdown>
                        {post.body}
                    </Markdown>
                }
                {isParley && embedable && embedable.embedable &&
                    <Iframe url={`https://api.parley.io/oembed?url=${jsonMetadata.appdata.parley.url}&format=html`}
                    width="100%"
                    height="450px"
                    id="myId"
                    className="myClassname"
                    display="initial"
                    position="relative"
                    allowFullScreen/>
                }
                {isParley && embedable && !embedable.embedable &&
                    <a href={videoUrl}>{videoUrl}</a>
                }

            </Col>
          </Row></div>;
    }

    let askContent = "";
    if (asksteemPosts) {
      askContent = <ul>
          {
            asksteemPosts.map((post) => {
              const url = `${post.author}/${post.permlink}`;
              return <li>
                <a href={`/thread/${url}`}>{ post.title.toString() }</a>
              </li>
            })
          }
      </ul>
    }

      let relativeContent = "";
      if (relativePosts) {
        relativeContent = <ul>
            {
                relativePosts.map((post) => {
                    const url = `${post.author}/${post.permlink}`;
                    return <li>
                      <a href={`/thread/${url}`}>{ post.title.toString() }</a>
                    </li>
                })
            }
        </ul>
      }

    return (
      <Wrapper title={post.title} search={post.parent_permlink} link={videoUrl}>
        <div className="sweet-loading">
          <ScaleLoader color={'#4A4A4A'} loading={loading} />
        </div>
        <div className="spacer"/>
        <Container>
          <Row>
            <Col sm="8">
              {content}
              <hr/>
              <Comments post={post}
                        replyHandler={replyHandler}/>
              <div className="spacer"/>
            </Col>
            <Col sm="4">
              <Card className="affix">
                {post.parent_permlink &&
                  <h3>More in { removeParleyFromTag(post.parent_permlink) }</h3>                  
                }
                <p>
                    {relativeContent}
                </p>
                {/*<h3>Related Posts</h3>*/}
                {/*<p>*/}
                    {/*{askContent}*/}
                {/*</p>*/}
              </Card>
            </Col>
          </Row>
        </Container>
      </Wrapper>
    );
  }
}

PostPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  post: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  asksteemPosts: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
  ]),
  relativePosts: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]),
};

export function mapDispatchToProps(dispatch) {
  return {
    loadPost: (author, permlink) => {
      dispatch(loadPost(author, permlink));
    },
    replyHandler: (payload) => {
      dispatch(postComment(payload));
    },
    saveAsksteemPosts: (posts) => {
      dispatch(saveAskSteemPosts(posts));
    },
    saveRelativePosts: (posts) => {
        dispatch(saveRelativePosts(posts));
    },
    setEmbedable: (payload) => {
        dispatch(checkEmbedable(payload));
    }
  };
}

const mapStateToProps = createStructuredSelector({
  post: makeSelectPost(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  asksteemPosts: makeSelectAsksteemPosts(),
  relativePosts: makeSelectRelativePosts(),
  embedable: makeSelectEmbedable(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'discussion', reducer });
const withSaga = injectSaga({ key: 'discussion', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(PostPage);
