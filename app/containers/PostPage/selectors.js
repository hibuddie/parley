import { createSelector } from 'reselect';

const selectPost = (state) => state.get('discussion');

const makeSelectPost = () => createSelector(
  selectPost,
  (postState) => postState.get('post')
);


const makeSelectLoading = () => createSelector(
  selectPost,
  (postState) => postState.get('loading')
);

const makeSelectError = () => createSelector(
  selectPost,
  (postState) => postState.get('error')
);


const makeSelectAsksteemPosts = () => createSelector(
    selectPost,
    (postState) => postState.get('asksteemPosts')
);

const makeSelectRelativePosts = () => createSelector(
    selectPost,
    (postState) => postState.get('relativePosts')
);

const makeSelectEmbedable = () => createSelector(
    selectPost,
    (postState) => postState.get('embedable')
);


export {
  makeSelectPost,
  makeSelectLoading,
  makeSelectError,
  makeSelectAsksteemPosts,
  makeSelectRelativePosts,
  makeSelectEmbedable,
};
