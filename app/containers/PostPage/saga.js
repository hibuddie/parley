import steem from 'steem';
import { STEEMIT_API_URL } from 'containers/App/constants';
import { call, put, takeLatest } from 'redux-saga/effects';
import { LOAD_POST, POST_COMMENT } from './constants';
import { postLoaded, postLoadingError, commentPosted, commentPostingError } from './actions';
import {delay} from 'utils/commen';

function connectContent(author, permlink) {
  return new Promise((resolve, reject) => {
    steem.api.setOptions({ url: STEEMIT_API_URL });
    steem.api.getContent(author, permlink,
      ((error, response) => {
        if (error) {
          reject(error);
        } else {
          resolve(response);
        }
      })
    );
  });
}

function connectReplies(author, permlink) {
  return new Promise((resolve, reject) => {
    steem.api.setOptions({ url: STEEMIT_API_URL });
    steem.api.getContentReplies(author, permlink,
      ((error, response) => {
        if (error) {
          reject(error);
        } else {
          resolve(response);
        }
      })
    );
  });
}

function connectPostComment(payload) {
  return new Promise((resolve, reject) => {
    steem.api.setOptions({ url: STEEMIT_API_URL });
    // steem.broadcast.comment(
    //   payload.wif, // posting wif
    //   payload.parentAuthor,
    //   payload.parentPermlink, // first tag
    //   payload.author, // username
    //   payload.permlink, // permlink
    //   '', // title
    //   payload.body, // body
    //   // json metadata (additional tags, app name)
    //   { app: 'parley' },
    //   (err, result) => {
    //     if (err) {
    //       reject(err);
    //     } else {
    //       resolve(result);
    //     }
    //   }
    // );

    const {wif, parentAuthor, parentPermlink, author, permlink, body} = payload;
      const operations = [
          ['comment',
              {
                  parent_author: parentAuthor,
                  parent_permlink: parentPermlink,
                  author,
                  permlink,
                  title: '',
                  body,
                  json_metadata : JSON.stringify({ app: 'parley' })
              }
          ],
          ['comment_options', {
              author,
              permlink,
              max_accepted_payout: '1000000.000 SBD',
              percent_steem_dollars: 10000,
              allow_votes: true,
              allow_curation_rewards: true,
              extensions: [
                  [0, {
                      beneficiaries: [
                          { account: 'hoxly', weight: 1000 }
                      ]
                  }]
              ]
          }]
      ];
      steem.broadcast.sendAsync(
          { operations, extensions: [] },
          { posting: wif },
          function(err, result){
              if (err) {
                  reject(err);
              } else {
                  resolve(result);
              }
          });
  });
}


function connectDiscussion(author, permlink) {
  steem.api.setOptions({ url: STEEMIT_API_URL });
  const comments = [];

  // Eagerly fetch all of the descendant replies (recursively)
  const fetchReplies = (author, permlink) => new Promise((resolve, reject) => {
    steem.api.getContentReplies(author, permlink,
        ((error, replies) => {
          if (error) {
            console.log(error);
          } else {
            const rr = replies.map((r) => {
              if (r.children > 0) {
                fetchReplies(r.author, r.permlink).then((children) => {
                  r.replies = children;
                  return r;
                });
              }
              return r;
            });
            resolve(rr);
          }
        })
      );
  });

  return new Promise((resolve, reject) => {
    steem.api.getContentAsync(author, permlink)
      .then((post) => fetchReplies(post.author, post.permlink)
        .then((comments) => {
          post.replies = comments;
          return post;
        }))
      .then((post) => {
        resolve(post);
      })
      .catch((error) => {
        reject(error);
      });
  });
}



export function* getPost(action) {
  try {
    // const post = yield call(connectContent, action.author, action.permlink);
    // const replies = yield call(connectReplies, action.author, action.permlink);
    const post = yield call(connectDiscussion, action.author, action.permlink);
    yield delay(1000);
    yield put(postLoaded(post));
  } catch (err) {
    yield put(postLoadingError(err));
  }
}

export function* postComment(action) {
  try {
    const comment = yield call(connectPostComment, action.payload);
    const { body, parentPermlink, parentAuthor, author, permlink } = action.payload;

    const currentdate = new Date();
    const newComment = { id: comment.id,
      body,
      parent_permlink: parentPermlink,
      parent_author: parentAuthor,
      author,
      permlink,
      net_votes: 0,
      created: currentdate.toString() };

    yield put(commentPosted(newComment));
  } catch (err) {
    yield put(commentPostingError(err));
  }
}

export default function* steemData() {
  // Watches for LOAD_REPOS actions and calls getPosts when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_POST, getPost);

  yield takeLatest(POST_COMMENT, postComment);
}

