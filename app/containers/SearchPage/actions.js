import {
    LOAD_ASKSTEEM_POSTS,
    LOAD_ASKSTEEM_POSTS_SUCCESS,
    LOAD_ASKSTEEM_POSTS_ERROR,
} from './constants';

export function loadPosts(query, currentPage) {
    return {
        type: LOAD_ASKSTEEM_POSTS,
        query,
        currentPage,
    };
}

export function postsLoaded(posts, hasMore, currentPage) {
    return {
        type: LOAD_ASKSTEEM_POSTS_SUCCESS,
        posts,
        hasMore,
        currentPage,
    };
}

export function postsLoadingError(error) {
    return {
        type: LOAD_ASKSTEEM_POSTS_ERROR,
        error,
    };
}

