import steem from 'steem';
import { call, put, takeLatest } from 'redux-saga/effects';
import { LOAD_ASKSTEEM_POSTS } from './constants';
import { postsLoaded, postsLoadingError } from './actions';
import request from 'utils/request';

export function* getPosts(action) {
    const requestURL = `https://api.asksteem.com/search?q=${action.query}&pg=${action.currentPage + 1}&types=post&include=payout`;

    try {
        // Call our request helper (see 'utils/request')
        const result = yield call(request, requestURL);
        yield put(postsLoaded(result.results, result.pages.has_next, result.pages.current));
    } catch (err) {
        yield put(postsLoadingError(err));
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* asksteemData() {
    // Watches for LOAD_REPOS actions and calls getPosts when one comes in.
    // By using `takeLatest` only the result of the latest API call is applied.
    // It returns task descriptor (just like fork) so we can continue execution
    // It will be cancelled automatically on component unmount
    yield takeLatest(LOAD_ASKSTEEM_POSTS, getPosts);
}

