import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import {
    Col,
    Container,
    Row,} from 'reactstrap';
import InfiniteScroll from 'react-infinite-scroller';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import Wrapper from 'containers/Wrapper/Loadable';
import {getParams} from "utils/commen";
import reducer from './reducer';
import saga from './saga';
import { makeSelectLocation } from 'containers/App/selectors'
import { makeSelectPosts, makeSelectCurrentPage, makeSelectHasMore, makeSelectLoading, makeSelectError } from './selectors';
import { loadPosts } from './actions';
import PostItem from 'components/PostItem';
import 'containers/HomePage/style.css';

export class SearchPage extends React.PureComponent {

    getSearchName = () => {
        const query = this.props.location.search;
        if (query !== '' ) {
            return getParams(query).q;
        }
        return ''
    };

    loadFunc = () => {
        if (this.props.loading || !this.props.hasMore) {
            return;
        }
        const {currentPage} = this.props;
        const query = getParams(this.props.location.search).q;

        this.props.loadPosts(query, currentPage);
    };

    render () {
        const searchName = this.getSearchName();
        const {posts, hasMore} = this.props;
        let content = '';
        if (posts) {
            content = posts.map((post) => (
                <PostItem post={post} key={post.permlink}/>
            ));
        }
        return (
            <Wrapper title={searchName} search={searchName} asksteem={true}>
                <Container className="margintop">
                    <Row>
                        <Col lg="12">
                            <InfiniteScroll
                                loadMore={this.loadFunc}
                                hasMore
                                loader={ this.props.hasMore && <div className="loader">Loading ...</div> }
                            >
                                {content}
                            </InfiniteScroll>
                        </Col>
                    </Row>
                </Container>
            </Wrapper>
        )
    }
}

SearchPage.propTypes = {
    loading: PropTypes.bool,
    error: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.bool,
    ]),
    posts: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.bool,
    ]),
    currentPage: PropTypes.number,
    hasMore: PropTypes.bool,
};

export function mapDispatchToProps(dispatch) {
    return {
        loadPosts: (query, currentPage = 0) => {
            dispatch(loadPosts(query, currentPage));
        },
    };
}

const mapStateToProps = createStructuredSelector({
    posts: makeSelectPosts(),
    loading: makeSelectLoading(),
    error: makeSelectError(),
    currentPage: makeSelectCurrentPage(),
    hasMore: makeSelectHasMore(),
    location: makeSelectLocation(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'search', reducer });
const withSaga = injectSaga({ key: 'search', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(SearchPage);