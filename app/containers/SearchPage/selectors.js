import { createSelector } from 'reselect';

const selectSearch = (state) => state.get('search');

const makeSelectPosts = () => createSelector(
    selectSearch,
    (searchState) => searchState.get('posts')
);

const makeSelectCurrentPage = () => createSelector(
    selectSearch,
    (searchState) => searchState.get('currentPage')
);

const makeSelectHasMore = () => createSelector(
    selectSearch,
    (searchState) => searchState.get('hasMore')
);

const makeSelectLoading = () => createSelector(
    selectSearch,
    (searchState) => searchState.get('loading')
);

const makeSelectError = () => createSelector(
    selectSearch,
    (searchState) => searchState.get('error')
);

export {
    selectSearch,
    makeSelectPosts,
    makeSelectCurrentPage,
    makeSelectHasMore,
    makeSelectLoading,
    makeSelectError,
};
