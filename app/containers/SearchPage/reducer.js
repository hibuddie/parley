import { fromJS } from 'immutable';

import {
    LOAD_ASKSTEEM_POSTS,
    LOAD_ASKSTEEM_POSTS_SUCCESS,
    LOAD_ASKSTEEM_POSTS_ERROR,
} from './constants';

const initState = fromJS({
    posts: false,
    currentPage: 0,
    hasMore: true,
    loading: false,
    error: false,
});

function searchReducer(state = initState, action) {
    switch (action.type) {
        case LOAD_ASKSTEEM_POSTS:
            return state.set('loading', true)
                .set('error', false)
                .set('currentPage', action.currentPage)
        case LOAD_ASKSTEEM_POSTS_SUCCESS:
        {
            let posts = state.get('posts');
            const currentPage = state.get('currentPage');
            if (currentPage === 0) {
                posts = action.posts;
            } else {
                action.posts.map((post, index) => {
                    posts.push(post);
                });
            }
            return state.set('loading', false)
                .set('currentPage', action.currentPage)
                .set('error', false)
                .set('hasMore', action.hasMore)
                .set('posts', posts);
        }
        case LOAD_ASKSTEEM_POSTS_ERROR:
            return state.set('loading', false)
                .set('error', action.error);
        default:
            return state;
    }
}

export default searchReducer;