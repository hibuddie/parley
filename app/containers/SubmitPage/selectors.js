import { createSelector } from 'reselect';

const selectSubmit = (state) => state.get('submit');

const makeSelectLoading = () => createSelector(
  selectSubmit,
  (submitState) => submitState.get('loading')
);

const makeSelectError = () => createSelector(
  selectSubmit,
  (submitState) => submitState.get('error')
);

const makeSelectPermlink = () => createSelector(
    selectSubmit,
    (submitState) => submitState.get('permlink')
);

const makeSelectSuccess = () => createSelector(
    selectSubmit,
    (submitState) => submitState.get('success')
);

export {
  selectSubmit,
  makeSelectLoading,
  makeSelectError,
    makeSelectPermlink,
    makeSelectSuccess,
};
