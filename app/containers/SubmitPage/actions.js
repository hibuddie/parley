import { POST_START, POST_SUCCESS, POST_ERROR, SHOW_ERROR } from './constants';

export function startPost(data) {
  return {
    type: POST_START,
    payload: data,
  };
}

export function postSucceed() {
  return {
    type: POST_SUCCESS,
  };
}

export function postError(error) {
  return {
    type: POST_ERROR,
    error,
  };
}

export function showError(error) {
    return {
        type: SHOW_ERROR,
        error,
    };
}
