import { fromJS } from 'immutable';

import { POST_SUCCESS, POST_START, POST_ERROR, SHOW_ERROR } from './constants';

const initState = fromJS({
  permlink: false,
  loading: false,
  error: false,
  success: false,
});

function submitReducer(state = initState, action) {
  switch (action.type) {
    case POST_START:
      return state.set('loading', true)
          .set('error', false)
          .set('success', false)
          .set('permlink', action.payload.permlink);
    case POST_SUCCESS:
      return state.set('loading', false)
          .set('success', true)
          .set('error', false);
    case POST_ERROR:
      return state.set('loading', false)
          .set('success', false)
          .set('error', action.error.data.message);
    case SHOW_ERROR:
      return state.set('error', action.error);
    default:
      return state;
  }
}

export default submitReducer;
