export const POST_START = 'Parley/SubmitPage/POST_START';
export const POST_SUCCESS = 'Parley/SubmitPage/POST_SUCCESS';
export const POST_ERROR = 'Parley/SubmitPage/POST_ERROR';
export const SHOW_ERROR = 'Parley/SubmitPage/SHOW_ERROR';
