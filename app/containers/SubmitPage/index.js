import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  Form,
  Input,
  Col,
  Container,
  Alert,
  Row } from 'reactstrap';
import { ScaleLoader} from 'react-spinners';
import isUrl from 'is-url';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import Wrapper from 'containers/Wrapper/Loadable';
import { startPost, showError } from './actions';
import reducer from './reducer';
import saga from './saga';
import { makeSelectPermlink, makeSelectLoading, makeSelectError, makeSelectSuccess } from './selectors';
import './style.css';
import { getCurrentUser } from 'utils/user';

export class Submit extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {
      const currentUser = getCurrentUser();
      const {permlink} = nextProps;
      if (nextProps.success) {
        window.location.href = `/thread/${currentUser.username}/${permlink}`
      }
  }

  _submit = () => {

    const currentUser = getCurrentUser();
    const url = document.getElementById('url').value;

    if (!isUrl(url)) {
      this.props.showError('Invalid Url');
      return;
    }

    const title = document.getElementById('title').value;
    if (title === '') {
        this.props.showError('Title must not be empty!');
        return;
    }
    const wif = currentUser.password;
    const firstTag = document.getElementById('community').value;
    if (firstTag === '') {
        this.props.showError('Community must not be empty!');
        return;
    }
    const username = currentUser.username;
    const permlink = `${new Date().toISOString().replace(/[^a-zA-Z0-9]+/g, '').toLowerCase()}-post`;

    const postPath = `https://www.parley.io/thread/${username}/${permlink}`;
    const body = `<center>![](https://api.parley.io/screenshot?url=${url})</center>
                  <center><h1> [View on Parley](${postPath}) </h1></center>
                  # What is Parley?
                  Parley is a Steem-based Reddit competitor that allows users to submit links and curate news they find around the web in order to promote discussion on the Steem platform. [Read more](https://steemit.com/parley/@thekyle/introducing-parley-a-decentralized-discussion-engine-based-on-steem)`;

    const data = { url, title, wif, firstTag, username, permlink, body };
    this.props.startPost(data);
  };

  render() {
    const {loading} = this.props;

    return (
      <Wrapper title="Submit">
        <div className="sweet-loading">
          <ScaleLoader
            color={'#4A4A4A'}
            loading={loading}
          />
        </div>
        <Container>
          <div className="spacer"></div>
            {this.props.error &&
            <Alert color="danger">
                {this.props.error}
            </Alert>
            }
          <Row>
            <Col md="6" sm="12" xs="12">
              <h1>Submit a Link</h1>
              <p>
                Have a link that you want to share with the world? Here's were to post it.
                <ul>
                  <li><b>URL</b> - the full URL to the page (should start with https or http)</li>
                  <li><b>Title</b> - a descriptive title to help other users get the gist of your post</li>
                  <li><b>Community</b> - the community you want to post this into</li>
                </ul>
              </p>
            </Col>
            <Col md="6" sm="12" xs="12">
              <Form>
                <Input type="text" placeholder="URL" name="url" id="url" required></Input>
                <br></br>
                <Input type="text" placeholder="Title" name="title" id="title" required></Input>
                <br></br>
                <Input type="text" placeholder="Community" name="community" id="community" required></Input>
                <br></br>
                <Input type="button" value="Post" onClick={this._submit} disabled={loading}></Input>
              </Form>
            </Col>
          </Row>
        </Container>
      </Wrapper>
    );
  }
}

Submit.PropTypes = {
  permlink: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.string,
  ]),
  loading: PropTypes.bool,
  success: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
};

export function mapDispatchToProps(dispatch) {
  return {
    startPost: (data) => {
      dispatch(startPost(data));
    },
    showError: (error) => {
        dispatch(showError(error));
    }
  };
}

const mapStateToProps = createStructuredSelector({
    loading: makeSelectLoading(),
    error: makeSelectError(),
    permlink: makeSelectPermlink(),
    success: makeSelectSuccess(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'submit', reducer });
const withSaga = injectSaga({ key: 'submit', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Submit);
