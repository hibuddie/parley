import steem from 'steem';

import { STEEMIT_API_URL } from 'containers/App/constants';

import { call, put, takeLatest } from 'redux-saga/effects';
import { POST_START } from './constants';
import { postSucceed, postError } from './actions';

const connect = (payload) => new Promise((resolve, reject) => {

  const {firstTag, username, wif, permlink, title, body, url} = payload;

  const metaData = { tags: [firstTag, `parley-${firstTag}`, 'parley'],
                    app: { name: 'parley', version: '1.0', website: {domain: 'www.parley.io', scheme: 'https', url: `/thread/${username}/${permlink}`} },
                    appdata: { parley: { url: url, nonce: 'tbd' } } };

  steem.api.setOptions({ url: STEEMIT_API_URL });
  // steem.broadcast.comment(
  //   payload.wif, // posting wif
  //   '',
  //   `parley-${payload.firstTag}`, // first tag
  //   payload.username, // username
  //   payload.permlink, // permlink
  //   payload.title, // title
  //   payload.body, // body
  //   // json metadata (additional tags, app name)
  //   metaData,
  //   (err, result) => {
  //     if (err) {
  //       reject(err);
  //     } else {
  //       resolve(result);
  //     }
  //   }
  // );

  const operations = [ 
         ['comment', 
           { 
             parent_author: '',
             parent_permlink: `parley-${firstTag}`,
             author: payload.username, 
             permlink, 
             title, 
             body, 
             json_metadata : JSON.stringify(metaData) 
           } 
         ], 
           ['comment_options', { 
             author: payload.username, 
             permlink, 
             max_accepted_payout: '1000000.000 SBD', 
             percent_steem_dollars: 10000,
             allow_votes: true, 
             allow_curation_rewards: true, 
             extensions: [ 
               [0, { 
                 beneficiaries: [ 
                   { account: 'hoxly', weight: 1000 }
                 ] 
               }] 
             ] 
           }] 
         ];
  steem.broadcast.sendAsync(
         { operations, extensions: [] },
         { posting: wif },
         function(err, result){
              if (err) {
                reject(err);
              } else {
                resolve(result);
              }
         });
});

export function* postLink(action) {
  try {
    yield call(connect, action.payload);
    yield put(postSucceed());
  } catch (err) {
    yield put(postError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* steemData() {
  // Watches for POST_START actions and calls getPosts when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(POST_START, postLink);
}
