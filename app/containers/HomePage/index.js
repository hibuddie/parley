/* eslint-disable react/style-prop-object,no-unused-vars,import/first */
/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import {
    Nav,
    NavItem,
    NavLink,
    Button,
    Card,
    ButtonGroup,
    Col,
    Container,
    Row,
    Jumbotron } from 'reactstrap';
import InfiniteScroll from 'react-infinite-scroller';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
import { makeSelectLocation, makeSelectTags } from 'containers/App/selectors'
import { makeSelectPosts, makeSelectCategory, makeSelectCurrentPage, makeSelectStartPermlink,
    makeSelectHasMore, makeSelectStartAuthor, makeSelectLoading, makeSelectError } from './selectors';
import { loadPosts } from './actions';
import { loadTags } from "containers/App/actions";
import PostItem from 'components/PostItem';
import Wrapper from 'containers/Wrapper/Loadable';
import { CATEGORY_TRENDING, CATEGORY_NEW, CATEGORY_FEED } from './constants';
import { getCurrentUser, setCurrentUser } from 'utils/user';
import {getParams, getParleyTag} from 'utils/commen';
import './style.css';
import Tagbox from 'components/Tagbox';


export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    // this.loadTags();
  }

  loadTags() {
      const currentUser = getCurrentUser();
      if (currentUser) {
          this.props.loadTags(currentUser.tags);
      }
  }

  getTag = () => {
      const {pathname} = this.props.location;
      if (pathname === '') {
          return ''
      }

      const splits = pathname.split('/');
      return splits[splits.length - 1];
  };

  loadFunc = () => {
    if (this.props.loading || !this.props.hasMore) {
      return;
    }
    let tagName = getParleyTag(this.getTag());
    const {currentPage, category, posts} = this.props;

    if (!currentPage) {
        this.props.loadPosts(currentPage, '', '', category, tagName);
        return
    }

    const post = posts[posts.length - 1];
    if (post) {
      const permlink = post.permlink;
      const author = post.author;
      const category = this.props.category;
      this.props.loadPosts(currentPage, permlink, author, category, tagName);
    }
  };

  navlinkClicked = (category) => {
    if (category === this.props.category) {
      return;
    }
    let tagName = getParleyTag(this.getTag());
    this.props.loadPosts(0, '', '', category, tagName);
  };

  deleteTag = (tag) => {
      const currentUser = getCurrentUser();
      if (currentUser) {
        const {username, password} = currentUser;
        let tags = currentUser.tags;
        tags = tags.filter(e => e !== tag);
        setCurrentUser(username, password, tags);
        this.props.loadTags(tags);
      }
  };

  render() {
    const { posts, category, tags } = this.props;
    let content = '';
    if (posts) {
      content = posts.map((post) => (
        <PostItem post={post} key={post.id}/>
      ));
    }
    let loginContent = '';
    if (getCurrentUser() === null) {
      loginContent = (<ButtonGroup>
        <Button color="success" href="/login">Login</Button>
        <Button color="primary" href="https://steemit.com/pick_account">Register</Button>
      </ButtonGroup>);
    }

    const tagName = this.getTag();
    const title = tagName ? tagName : 'Home';

    return (
      <Wrapper title={title} search={this.getTag()}>
          { tagName === '' &&
            <Jumbotron className="welcome" fluid>
              <Container>
                <center>
                  <h1 className="display-3 site-title">Parley</h1>
                  <p className="lead" style={{ color: '#FFF' }}>The place where conversations happen.</p>
                  {loginContent}
                </center>
              </Container>
            </Jumbotron>
          }
        <Container className={tagName !== '' ? 'margintop' : null}>
          <Row>
            <Col lg="2">
              <Nav vertical pills>
                <NavItem>
                  <NavLink onClick={() => this.navlinkClicked(CATEGORY_TRENDING)} active={category === CATEGORY_TRENDING}>Trending</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink onClick={() => this.navlinkClicked(CATEGORY_NEW)} active={category === CATEGORY_NEW}>New</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink onClick={() => this.navlinkClicked(CATEGORY_FEED)} active={category === CATEGORY_FEED}>Hot</NavLink>
                </NavItem>
              </Nav>
            </Col>
            <Col lg="8">
                { Array.isArray(posts) && posts.length === 0 &&
                    <div className="no-results">No Results</div>
                }
              <InfiniteScroll
                loadMore={this.loadFunc}
                hasMore
                loader={ this.props.hasMore && <div className="loader">Loading ...</div> }
              >
                {content}
              </InfiniteScroll>
            </Col>
            <Col lg="2">
                <Tagbox/>
            </Col>
          </Row>
        </Container>
      </Wrapper>
    );
  }
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  posts: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]),
  currentPage: PropTypes.number,
  startPermlink: PropTypes.string,
  category: PropTypes.string,
  hasMore: PropTypes.bool,
  tags: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.bool,
  ]),
};

export function mapDispatchToProps(dispatch) {
  return {
    loadPosts: (currentPage = 0, startPermlink = '', startAuthor = '', category = CATEGORY_TRENDING, tagName='') => {
      dispatch(loadPosts(currentPage, startPermlink, startAuthor, category, tagName));
    },

    loadTags: (tags) => {
      dispatch(loadTags(tags));
    },

    deleteTag: (tag) => {
      dispatch(deleteTag(tag));
    },
  };
}

const mapStateToProps = createStructuredSelector({
  posts: makeSelectPosts(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  currentPage: makeSelectCurrentPage(),
  startPermlink: makeSelectStartPermlink(),
  category: makeSelectCategory(),
  hasMore: makeSelectHasMore(),
  startAuthor: makeSelectStartAuthor(),
  location: makeSelectLocation(),
  tags: makeSelectTags(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
