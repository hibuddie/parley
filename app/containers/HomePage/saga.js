import steem from 'steem';
import { call, put, takeLatest } from 'redux-saga/effects';
import { LOAD_POSTS } from './constants';
import { postsLoaded, postsLoadingError } from './actions';
import { CATEGORY_TRENDING, CATEGORY_FEED, CATEGORY_NEW } from './constants';
import {STEEMIT_API_URL} from "../App/constants";

function connect(start_permlink, start_author, category, tagName) {
  return new Promise((resolve, reject) => {
    let query = { limit: 20 };

    query = { limit: 20, tag: tagName };

    if (start_permlink !== '') {
        query['start_permlink'] = start_permlink;
        query['start_author'] = start_author;
    }

    if (tagName !== '') {
        query['tag'] = tagName;
    } else {
        query['tag'] = 'parley';
    }

    steem.api.setOptions({ url: STEEMIT_API_URL });
    switch (category) {
      case CATEGORY_TRENDING:
        steem.api.getDiscussionsByTrending(query,
          ((error, response) => {
            if (error) {
              reject(error);
            } else {
              resolve(response);
            }
          })
        );
        break;
      case CATEGORY_NEW:
        steem.api.getDiscussionsByCreated(query,
          ((error, response) => {
            if (error) {
              reject(error);
            } else {
              resolve(response);
            }
          })
        );
        break;
      case CATEGORY_FEED:
        steem.api.getDiscussionsByHot(query,
          ((error, response) => {
            if (error) {
              reject(error);
            } else {
              resolve(response);
            }
          })
        );
        break;
      default:
        steem.api.getDiscussionsByTrending(query,
          ((error, response) => {
            if (error) {
              reject(error);
            } else {
              resolve(response);
            }
          })
        );
    }
  });
}


export function* getPosts(action) {
  try {
    const posts = yield call(connect, action.startPermlink, action.startAuthor, action.category, action.tagName);
    yield put(postsLoaded(posts));
  } catch (err) {
    yield put(postsLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* steemData() {
  // Watches for LOAD_REPOS actions and calls getPosts when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_POSTS, getPosts);
}

