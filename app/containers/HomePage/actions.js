
import {
  LOAD_POSTS,
  LOAD_POSTS_SUCCESS,
  LOAD_POSTS_ERROR,
} from './constants';


export function loadPosts(currentPage, startPermlink, startAuthor, category, tagName) {
  return {
    type: LOAD_POSTS,
    currentPage,
    startPermlink,
    startAuthor,
    category,
    tagName,
  };
}

export function postsLoaded(posts) {
    const arr = [];
    posts.map(p => {
        const jsonMetadata = JSON.parse(p.json_metadata);
        if (jsonMetadata.app && jsonMetadata.app.name && jsonMetadata.app.name === 'parley') {
            arr.push(p);
        }
    });
    console.log('arr', arr);
  const hasMore = posts.length >= 20;
  return {
    type: LOAD_POSTS_SUCCESS,
    posts: arr,
    hasMore,
  };
}

export function postsLoadingError(error) {
  return {
    type: LOAD_POSTS_ERROR,
    error,
  };
}

