export const LOAD_POSTS = 'Parley/Home/LOAD_POSTS';
export const LOAD_POSTS_SUCCESS = 'Parley/Home/LOAD_POSTS_SUCCESS';
export const LOAD_POSTS_ERROR = 'Parley/Home/LOAD_POSTS_ERROR';
export const CATEGORY_TRENDING = 'Parley/Home/CATEGORY_TRENDING';
export const CATEGORY_FEED = 'Parley/Home/CATEGORY_FEED';
export const CATEGORY_NEW = 'Parley/Home/CATEGORY_NEW';


