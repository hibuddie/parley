import { fromJS } from 'immutable';


import {
  LOAD_POSTS,
  LOAD_POSTS_SUCCESS,
  LOAD_POSTS_ERROR,
  CATEGORY_TRENDING,
  CATEGORY_FEED,
  CATEGORY_NEW,
} from './constants';

const initState = fromJS({
  posts: false,
  category: CATEGORY_TRENDING,
  startPermlink: '',
  startAuthor: '',
  currentPage: 0,
  hasMore: true,
  loading: false,
  error: false,
});

function homeReducer(state = initState, action) {
  switch (action.type) {
    case LOAD_POSTS:
      return state.set('loading', true)
        .set('error', false)
        .set('startPermlink', action.startPermlink)
        .set('startAuthor', action.startAuthor)
        .set('currentPage', action.currentPage)
        .set('category', action.category);
    case LOAD_POSTS_SUCCESS:
      {
        let posts = state.get('posts');
        const currentPage = state.get('currentPage');
        if (currentPage === 0) {
            posts = action.posts;
        } else {
            posts = posts.concat(action.posts.slice(1));
        }

        return state.set('loading', false)
            .set('currentPage', currentPage + 1)
            .set('error', false)
            .set('posts', posts)
            .set('hasMore', action.hasMore);
      }
    case LOAD_POSTS_ERROR:
      return state.set('loading', false)
        .set('error', action.error);
    default:
      return state;
  }
}

export default homeReducer;
