import { createSelector } from 'reselect';

const selectHome = (state) => state.get('home');

const makeSelectPosts = () => createSelector(
  selectHome,
  (homeState) => homeState.get('posts')
);

const makeSelectStartPermlink = () => createSelector(
  selectHome,
  (homeState) => homeState.get('startPermlink')
);

const makeSelectStartAuthor = () => createSelector(
  selectHome,
  (homeState) => homeState.get('startAuthor')
);

const makeSelectCategory = () => createSelector(
  selectHome,
  (homeState) => homeState.get('category')
);

const makeSelectCurrentPage = () => createSelector(
  selectHome,
  (homeState) => homeState.get('currentPage')
);

const makeSelectHasMore = () => createSelector(
  selectHome,
  (homeState) => homeState.get('hasMore')
);

const makeSelectLoading = () => createSelector(
  selectHome,
  (homeState) => homeState.get('loading')
);

const makeSelectError = () => createSelector(
  selectHome,
  (homeState) => homeState.get('error')
);

export {
  selectHome,
  makeSelectPosts,
  makeSelectCategory,
  makeSelectStartPermlink,
  makeSelectCurrentPage,
  makeSelectHasMore,
  makeSelectStartAuthor,
  makeSelectLoading,
  makeSelectError,
};
