import steem from 'steem';

import { STEEMIT_API_URL } from 'containers/App/constants';

import { call, put, takeLatest } from 'redux-saga/effects';
import { LOAD_AUTHOR, LOAD_AUTHOR_POSTS } from './constants';
import { authorLoaded, authorLoadingError, postsLoaded, postsLoadingError } from './actions';
import {delay} from "utils/commen";

const connect = (authorName) => {
    steem.api.setOptions({ url: STEEMIT_API_URL });

    return new Promise((resolve, reject) => {
        steem.api.getAccounts(
            [authorName],
            (err, [userAccount]) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(userAccount);
                }
            }
        );
    });
};

const connectPosts = (authorName, startPermlink) => {
    steem.api.setOptions({ url: STEEMIT_API_URL });
    const beforeDate=new Date().toISOString().split('.')[0];

    return new Promise((resolve, reject) => {
        steem.api.getDiscussionsByAuthorBeforeDate(
            authorName, startPermlink, beforeDate, 20,
            (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            }
        );
    });
};

export function* getAuthor(action) {
    try {
        const author = yield call(connect, action.authorName);
        yield put(authorLoaded(author));
    } catch (err) {
        yield put(authorLoadingError(err));
    }
}

export function* getAuthorPosts(action) {
    try {
        const posts = yield call(connectPosts, action.authorName, action.startPermlink);
        yield put(postsLoaded(posts));
    } catch (err) {
        yield put(postsLoadingError(err));
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* authorData() {
    // Watches for LOAD_REPOS actions and calls getPosts when one comes in.
    // By using `takeLatest` only the result of the latest API call is applied.
    // It returns task descriptor (just like fork) so we can continue execution
    // It will be cancelled automatically on component unmount
    yield takeLatest(LOAD_AUTHOR, getAuthor);

    yield takeLatest(LOAD_AUTHOR_POSTS, getAuthorPosts);
}

