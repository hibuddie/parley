import { createSelector } from 'reselect';

const selectAuthor = (state) => state.get('author');

const makeSelectAuthor = () => createSelector(
    selectAuthor,
    (authorState) => authorState.get('data')
);

const makeSelectLoading = () => createSelector(
    selectAuthor,
    (authorState) => authorState.get('loading')
);

const makeSelectError = () => createSelector(
    selectAuthor,
    (authorState) => authorState.get('error')
);

const makeSelectPosts = () => createSelector(
    selectAuthor,
    (authorState) => authorState.get('posts')
);

const makeSelectCurrentPage = () => createSelector(
    selectAuthor,
    (authorState) => authorState.get('currentPage')
);

const makeSelectHasMore = () => createSelector(
    selectAuthor,
    (authorState) => authorState.get('hasMore')
);

const makeSelectALoading = () => createSelector(
    selectAuthor,
    (authorState) => authorState.get('aLoading')
);

const makeSelectAError = () => createSelector(
    selectAuthor,
    (authorState) => authorState.get('aError')
);

const makeSelectStartPermlink = () => createSelector(
    selectAuthor,
    (authorState) => authorState.get('startPermlink')
);

export {
    selectAuthor,
    makeSelectAuthor,
    makeSelectLoading,
    makeSelectError,
    makeSelectALoading,
    makeSelectAError,
    makeSelectCurrentPage,
    makeSelectPosts,
    makeSelectStartPermlink,
    makeSelectHasMore,



};
