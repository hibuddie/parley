import {
    LOAD_AUTHOR,
    LOAD_AUTHOR_SUCCESS,
    LOAD_AUTHOR_ERROR,
    LOAD_AUTHOR_POSTS,
    LOAD_AUTHOR_POSTS_SUCCESS,
    LOAD_AUTHOR_POSTS_ERROR,
} from './constants';

export function loadAuthor(authorName) {
    return {
        type: LOAD_AUTHOR,
        authorName,
    };
}

export function authorLoaded(author) {
    return {
        type: LOAD_AUTHOR_SUCCESS,
        author,
    };
}

export function authorLoadingError(error) {
    return {
        type: LOAD_AUTHOR_ERROR,
        error,
    };
}

export function loadPosts(authorName, startPermlink, currentPage) {
    return {
        type: LOAD_AUTHOR_POSTS,
        authorName,
        startPermlink,
        currentPage,
    };
}

export function postsLoaded(posts) {
    const hasMore = posts.length >= 20;
    return {
        type: LOAD_AUTHOR_POSTS_SUCCESS,
        posts,
        hasMore,
    };
}

export function postsLoadingError(error) {
    return {
        type: LOAD_AUTHOR_POSTS_ERROR,
        error,
    };
}


