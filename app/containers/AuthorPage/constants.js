export const LOAD_AUTHOR = 'Parley/AuthorPage/LOAD_AUTHOR';
export const LOAD_AUTHOR_SUCCESS = 'Parley/AuthorPage/LOAD_AUTHOR_SUCCESS';
export const LOAD_AUTHOR_ERROR = 'Parley/AuthorPage/LOAD_AUTHOR_ERROR';
export const LOAD_AUTHOR_POSTS = 'Parley/AuthorPage/LOAD_AUTHOR_POSTS';
export const LOAD_AUTHOR_POSTS_SUCCESS = 'Parley/AuthorPage/LOAD_AUTHOR_POSTS_SUCCESS';
export const LOAD_AUTHOR_POSTS_ERROR = 'Parley/AuthorPage/LOAD_AUTHOR_POSTS_ERROR';