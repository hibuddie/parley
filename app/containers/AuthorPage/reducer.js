import { fromJS } from 'immutable';

import {
    LOAD_AUTHOR,
    LOAD_AUTHOR_SUCCESS,
    LOAD_AUTHOR_ERROR,
    LOAD_AUTHOR_POSTS,
    LOAD_AUTHOR_POSTS_SUCCESS,
    LOAD_AUTHOR_POSTS_ERROR,
} from './constants';

const initState = fromJS({
    data: false,
    aLoading: false,
    aError: false,
    loading: false,
    error: false,
    currentPage: 0,
    hasMore: true,
    startPermlink: '',
});

function authorReducer(state = initState, action) {
    switch (action.type) {
        case LOAD_AUTHOR:
            return state.set('aLoading', true)
                .set('aError', false);
        case LOAD_AUTHOR_SUCCESS:
            return state.set('aLoading', false)
                .set('aError', false)
                .set('data', action.author);
        case LOAD_AUTHOR_ERROR:
            return state.set('aLoading', false)
                .set('aError', action.error);
        case LOAD_AUTHOR_POSTS:
            return state.set('loading', true)
                .set('error', false)
                .set('currentPage', action.currentPage)
                .set('startPermlink', action.startPermlink);
        case LOAD_AUTHOR_POSTS_SUCCESS:
        {
            let posts = state.get('posts');
            const currentPage = state.get('currentPage');
            if (currentPage === 0) {
                posts = action.posts;
            } else {
                posts = posts.concat(action.posts.slice(1));
            }
            return state.set('loading', false)
                .set('currentPage', currentPage + 1)
                .set('error', false)
                .set('posts', posts)
                .set('hasMore', action.hasMore);
        }
        case LOAD_AUTHOR_POSTS_ERROR:
            return state.set('loading', false)
                .set('error', action.error);
        default:
            return state;
    }
}

export default authorReducer;