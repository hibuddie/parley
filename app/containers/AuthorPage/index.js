import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import {
    Col,
    Container,
    Card,
    Row,} from 'reactstrap';
import InfiniteScroll from 'react-infinite-scroller';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import Wrapper from 'containers/Wrapper/Loadable';
import reducer from './reducer';
import saga from './saga';
import { makeSelectLocation } from 'containers/App/selectors'
import { makeSelectAuthor, makeSelectLoading, makeSelectError, makeSelectAError,
    makeSelectALoading, makeSelectPosts, makeSelectCurrentPage, makeSelectHasMore, makeSelectStartPermlink } from './selectors';
import { loadAuthor, loadPosts } from './actions';
import PostItem from 'components/PostItem';
import 'containers/HomePage/style.css';


export class AuthorPage extends React.PureComponent {

    componentDidMount() {
        const authorName = this.getAuthorName();
        this.props.loadAuthor(authorName);
    }

    getAuthorName = () => {
        const {pathname} = this.props.location;
        const splits = pathname.split('/');
        return splits[splits.length - 1];
    };

    loadFunc = () => {
        const authorName = this.getAuthorName();
        if (this.props.loading || !this.props.hasMore) {
            return;
        }
        const {currentPage, posts} = this.props;

        if (!currentPage) {
            this.props.loadPosts(authorName, null, currentPage);
            return
        }

        const post = posts[posts.length - 1];
        if (post) {
            const permlink = post.permlink;
            this.props.loadPosts(authorName, permlink, currentPage);
        }
    };

    render () {
        const {data, loading, error} = this.props;
        const {posts, hasMore} = this.props;
        let content = '';
        if (posts) {
            content = posts.map((post) => {
                const jsonMetadata = JSON.parse(post.json_metadata);
                if (jsonMetadata.app && jsonMetadata.app.name && jsonMetadata.app.name === 'parley') {
                    return <PostItem post={post} key={post.permlink}/>
                } else {
                    return ''
                }
            });
        }

        let profile = '';
        let jsonMetaData = false;
        if (data) {
            jsonMetaData = JSON.parse(data.json_metadata);
        }

        const title = jsonMetaData ? jsonMetaData.profile.name : '';

        return (
            <Wrapper title={title}>
                <Container className="margintop">
                    <Row>
                        <Col lg="8">
                            <InfiniteScroll
                                loadMore={this.loadFunc}
                                hasMore
                                loader={ this.props.hasMore && <div className="loader">Loading ...</div> }
                            >
                                {content}
                            </InfiniteScroll>
                        </Col>
                        <Col lg="4">
                            {jsonMetaData &&
                            <Card className="affix">
                                <h4 style={{textAlign: 'center'}}>{jsonMetaData.profile.name}</h4>

                                <img src={jsonMetaData.profile.profile_image} className="img-profile" />

                                <span style={{marginTop: '10px'}}><strong>Location:</strong></span>
                                <span>{jsonMetaData.profile.location}</span>

                                <span><strong>About:</strong></span>
                                <span>{jsonMetaData.profile.about}</span>

                                { jsonMetaData.profile.website &&
                                    <div>
                                        <span><strong>Website:</strong></span><br/>
                                        <a href={jsonMetaData.profile.website}>{jsonMetaData.profile.website}</a>
                                    </div>


                                }
                            </Card>
                            }
                        </Col>
                    </Row>
                </Container>
            </Wrapper>
        )
    }
}

AuthorPage.propTypes = {
    aLoading: PropTypes.bool,
    aError: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.bool,
    ]),
    data: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.bool,
    ]),
    loading: PropTypes.bool,
    error: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.bool,
    ]),
    posts: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.bool,
    ]),
    currentPage: PropTypes.number,
    startPermlink: PropTypes.string,
    hasMore: PropTypes.bool,
};

export function mapDispatchToProps(dispatch) {
    return {
        loadAuthor: (authorName) => {
            dispatch(loadAuthor(authorName));
        },
        loadPosts: (authorName, startPermlink, currentPage ) => {
            dispatch(loadPosts(authorName, startPermlink, currentPage))
        },
    };
}

const mapStateToProps = createStructuredSelector({
    data: makeSelectAuthor(),
    aLoading: makeSelectALoading(),
    aError: makeSelectAError(),
    location: makeSelectLocation(),
    posts: makeSelectPosts(),
    loading: makeSelectLoading(),
    error: makeSelectError(),
    currentPage: makeSelectCurrentPage(),
    startPermlink: makeSelectStartPermlink(),
    hasMore: makeSelectHasMore(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'author', reducer });
const withSaga = injectSaga({ key: 'author', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(AuthorPage);