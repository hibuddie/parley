import { fromJS } from 'immutable';
import {LOAD_TAGS} from "./constants";

// The initial state of the App
const initialState = fromJS({
    tags: false
});

function appReducer(state = initialState, action) {
  switch (action.type) {
      case LOAD_TAGS:
        return state.set('tags', action.tags);
     default:
        return state;
  }
}

export default appReducer;
