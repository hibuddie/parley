import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('global');
const selectRoute = (state) => state.get('route');

const makeSelectLocation = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('location').toJS()
);

const makeSelectTags = () => createSelector(
    selectGlobal,
    (globalState) => globalState.get('tags')
);

export {
    makeSelectTags,
    makeSelectLocation,
    selectGlobal,
};
