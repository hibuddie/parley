/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import HomePage from 'containers/HomePage/Loadable';
import SearchPage from 'containers/SearchPage/Loadable';
import Login from 'containers/LoginPage/Loadable';
import Submit from 'containers/SubmitPage/Loadable';
import Post from 'containers/PostPage/Loadable';
import AuthorPage from 'containers/AuthorPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import 'bootstrap/dist/css/bootstrap.css';

export default function App() {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/submit" component={Submit} />
        <Route path="/thread/:author/:permlink" component={Post} />
        <Route path="/user/:username" component={AuthorPage} />
        <Route path="/community/:tag" component={HomePage} />
        <Route exact path="/search" component={SearchPage}/>
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  );
}

