import { LOAD_TAGS} from "./constants";

export function loadTags(tags) {
    return {
        type: LOAD_TAGS,
        tags,
    }
}
