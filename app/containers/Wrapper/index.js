/* eslint-disable react/prop-types,no-unused-vars */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import steem from 'steem';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button,
  Link,
  Form,
  Input,
  Container } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import { getCurrentUser, setCurrentUser } from 'utils/user';
import './style.css';
import { CURRENT_USER } from 'utils/constants';
import {STEEMIT_API_URL} from "containers/App/constants";
import {loadTags} from "containers/App/actions";
import {getParleyTag, removeParleyFromTag} from "utils/commen";

export class Wrapper extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      search: props.search,
    };
  }

  componentDidMount() {
    this.updateTitle();

    if (this.props.asksteem) {
        window.document.getElementById('search').value = this.props.search;
    }
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  componentDidUpdate() {
      this.updateTitle();
  }

  updateTitle = () => {
      if (this.props.title) {
          document.title = this.props.title + " - Parley";
      } else {
          document.title = "Parley";
      }
  };

  changeHandler = () => {
      const search = document.getElementById('search').value;
      this.setState({search})
  };

  followTag = () => {
    const currentUser = getCurrentUser();
    const {username, password, tags} = currentUser;
    const {search} = this.state;
    if (search && !tags.includes(search)) {
      tags.push(search);
      setCurrentUser(username, password, tags);
      this.props.loadTags(tags);
    }
  };

  render() {
    let dropdownContent = '';
    const currentUser = getCurrentUser();
    if (currentUser === null) {
      dropdownContent = (<DropdownMenu >
        <DropdownItem>
          <div onClick={() => window.location.href = '/login' }>Login</div>
        </DropdownItem>
        <DropdownItem>
          <div onClick={() => window.location.href = 'https://steemit.com/pick_account' }>Register</div>
        </DropdownItem>
        <DropdownItem divider />
      </DropdownMenu>);
    } else {
      dropdownContent = (<DropdownMenu >
        <DropdownItem>
          <div onClick={() => window.location.href = `/user/${currentUser.username}` }>My Profile</div>
        </DropdownItem>
        <DropdownItem>
          <div onClick={() => {localStorage.removeItem(CURRENT_USER); location.href="/login"; }}>Logout</div>
        </DropdownItem>
      </DropdownMenu>);
    }
    const {search, asksteem} = this.props;
    let title = 'Parley';
    if (!asksteem && search && search !== '') {
      // const link = this.props.link ? this.props.link : `/community/${removeParleyFromTag(search)}`;
      const link = `/community/${removeParleyFromTag(search)}`;
      title = <span><a href="/">Parley</a> | <a href ={link}>{removeParleyFromTag(search)}</a></span>
    }

    return (
      <div>
        <Navbar color="faded" light expand="md">
          <NavbarBrand href="/" className="site-title">{title}</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="nav-center">
              <NavItem className="nav-search">
                <div className="searchbox" style={{display: 'none'}}>
                  <Form action="/search?">
                    <Input type="search" name="q" id="search" placeholder="Search..." title="Search for things" onChange={this.changeHandler} required/>
                    <button type="submit" value="Search..." className="icon">
                      <FontAwesome name="search"></FontAwesome>
                    </button>
                  </Form>
                </div>
              </NavItem>
              { search && currentUser &&
              <NavItem style={{display: 'none'}}>
                <Button outline style={{margin: '6px'}} onClick={this.followTag}>Follow</Button>
              </NavItem>
              }
            </Nav>
            <Nav className="ml-auto" navbar>
                {currentUser &&
                <NavItem>
                  <NavLink href="/submit">Submit Link</NavLink>
                </NavItem>
                }
              <UncontrolledDropdown nav>
                <DropdownToggle nav caret>
                  Account
                </DropdownToggle>
                {dropdownContent}
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
        <div>
          {this.props.children}
        </div>
        <footer className="footer">
          <Container>
            <span className="text-muted">Copyright 2017-{(new Date().getFullYear())} Hoxly, Inc. All Rights Reserved</span>
          </Container>
        </footer>
      </div>
    );
  }
}


Wrapper.propTypes = {
    title: PropTypes.string,
    search: PropTypes.string,
    asksteem: PropTypes.bool,
    link: PropTypes.string,
};

export function mapDispatchToProps(dispatch) {
    return {
      loadTags: (tags) => {
        dispatch(loadTags(tags));
      }
    };
}

const mapStateToProps = (state) => {
    return {}
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
    withConnect,
)(Wrapper);

