import React from 'react';
import { createStructuredSelector } from 'reselect';
import steem from 'steem';
import {
  Form,
  Input,
  Col,
  Container,
  Alert,
  Row } from 'reactstrap';
import { setCurrentUser } from "utils/user";
import Wrapper from 'containers/Wrapper/Loadable';
import {STEEMIT_API_URL} from "containers/App/constants";

export class Login extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
        'loading': false,
        'error': false,
    };
  }

  submitLogin = (event) => {
    event.preventDefault();
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    this.setState({loading: true});

    steem.api.setOptions({ url: STEEMIT_API_URL });

    const isValidUsername = steem.utils.validateAccountName(username);
    if (isValidUsername !== null) {
        this.setState({loading: false, error: 'Invalid username!'});
        return;
    }

    const isWif = steem.auth.isWif(password);
    if (!isWif) {
      this.setState({loading: false, error: 'Invalid Private Posting Key!'});
      return;
    }

    // Follow to @hoxly
    let followReq = ["follow"];
    followReq.push({follower: username, following: 'hoxly', what: ["blog"]})

    const customJson = JSON.stringify(followReq)

    steem.broadcast.customJsonAsync(password, [], [username], "follow", customJson)
      .then(result => {
        setCurrentUser(username, password, []);
        window.location.href = '/';
      }).catch(e => {
        setCurrentUser(username, password, []);
        window.location.href = '/';
      });
  };

  render() {
    return (
      <Wrapper title="Login" >
        <Container>
          <div className="spacer"></div>
            {this.state.error &&
            <Alert color="danger">
                {this.state.error}
            </Alert>
            }
          <Row>
            <Col md="6" sm="12" xs="12">
              <h1>Login</h1>
              <p>
                Have a link that you want to share with the world? Here's were to post it.
                <ul>
                  <li><b>Username</b> - your username on STEEM</li>
                  {/*<li><b>Password</b> - your STEEM master password</li>*/}
                  <li><b>Private Posting Key</b> - you can find this in Steemit > Your Profile > Wallet > Permissions > Posting > Show Private Key</li>
                </ul>
              </p>
            </Col>
            <Col md="6" sm="12" xs="12">
              <Form id="login-form" onSubmit={this.submitLogin}>
                <Input type="text" placeholder="Username" name="username" id="username" required></Input>
                <br></br>
                <Input type="password" placeholder="Password or Private Posting Key" name="password" id="password" required></Input>
                <br></br>
                <Input type="button" value="Login" onClick={this.submitLogin} disabled={this.state.loading} style={{cursor: 'pointer'}}></Input>
              </Form>
              <br></br>
              <center>Don't have an account? <a href="https://steemit.com/pick_account">Sign up</a>!</center>
            </Col>
          </Row>
        </Container>
      </Wrapper>
    );
  }
}

export default Login;
