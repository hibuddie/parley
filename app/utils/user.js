import { CURRENT_USER } from './constants';

export function setCurrentUser(username, password, tags) {
  const currentUser = { username, password, tags };
  localStorage.setItem(CURRENT_USER, JSON.stringify(currentUser));
}

export function getCurrentUser() {
  if (localStorage.getItem(CURRENT_USER)) {
    return JSON.parse(localStorage.getItem(CURRENT_USER));
  }
  return null;
}
