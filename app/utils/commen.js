export const getParams = query => {
    if (!query) {
        return { };
    }

    return (/^[?#]/.test(query) ? query.slice(1) : query)
        .split('&')
        .reduce((params, param) => {
            let [ key, value ] = param.split('=');
            params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
            return params;
        }, { });
};

export function delay(ms) {
    return new Promise((resolve) => setTimeout(() => resolve(true), ms));
}

export function calcPayout(post) {
    if (post.payout !== undefined) {
        return `${post.payout} SBD`;
    }

    if (post.total_payout_value === undefined || post.pending_payout_value === undefined ) {
        return "0 SBD";
    }
    const total = parseInt(post.total_payout_value.replace(' SBD', ''));
    const pending = parseInt(post.pending_payout_value.replace(' SBD', ''));
    return total > pending ? post.total_payout_value : post.pending_payout_value;
}

export function getParleyTag(tagName) {
    if (tagName !== '') {
        if (tagName.includes('parley-')) {
            return tagName;
        } else {
            return `parley-${tagName}`;
        }
    }
    return '';
}

export function addCommentToPost(post, comment) {
    const p = post;
    if (p.permlink === comment.parent_permlink) {
        if (p.replies) {
            const filtered = p.replies.filter(r => r.permlink === comment.permlink);
            if (filtered.length > 0) {
                p.replies = p.replies.map(r => {
                    if (r.permlink === comment.permlink) {
                        return comment;
                    } else {
                        return r;
                    }
                });
            } else {
                p.replies.push(comment);
            }
        }
        return p;
    }
    const replies = p.replies;
    if (p.replies)
        p.replies = replies.map((reply) => addCommentToPost(reply, comment));
    return p;
}

export function removeParleyFromTag(tag) {
    return tag.replace('parley-', '')
}