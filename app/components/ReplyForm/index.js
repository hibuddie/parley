import React from 'react';
import PropTypes from 'prop-types';
import { getCurrentUser } from 'utils/user';

export class ReplyForm extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {isOpen: false}
  }

  openReply = () => {
      this.setState({
          isOpen: !this.state.isOpen,
      });
  };
  send = () => {
    const { parentReply, replyHandler } = this.props;
    const parentPermlink = parentReply.permlink;

    const currentUser = getCurrentUser();
    const permlink = `${new Date().toISOString().replace(/[^a-zA-Z0-9]+/g, '').toLowerCase()}-post`;
    const author = currentUser.username;
    const wif = currentUser.password;
    const body = document.getElementById(parentPermlink).innerHTML;
    const payload = { body,
        parentPermlink,
        parentAuthor:parentReply.author,
        author, permlink, wif };
    replyHandler(payload);
    document.getElementById(parentPermlink).innerHTML = "";
    this.setState({isOpen: false});
  };

  clean = () => {
      const { parentReply } = this.props;
      document.getElementById(parentReply.permlink).innerHTML = "";
  };

  render() {
    const {isSubReply, parentReply} = this.props;
    const parentPermlink = parentReply.permlink;
    if (getCurrentUser() === null || !parentReply.allow_replies ) {
      return (
          <div></div>
      )
    }
    return (
        <div>
          { getCurrentUser() !== null && isSubReply &&
              <span className="actions">
                <button className="action reply" type="button" onClick={this.openReply}>Reply</button>
              </span>}
            {(this.state.isOpen || !isSubReply) &&
            <div className="commenting-field main">
              <i className="fa fa-user profile-picture round" />
              <div className="textarea-wrapper">
                <span className="close inline-button" onClick={this.clean}><span className="left" /><span className="right" /></span>
                <div className="textarea" data-placeholder="Add a comment" contentEditable="true" id={parentPermlink} style={{ height: '3.65em' }} />
                <div className="control-row"><button onClick={this.send} style={{ float: 'right', cursor: 'pointer' }}>Send</button></div>
              </div>
            </div>}
        </div>
    );

  }
}

ReplyForm.propTypes = {
  parentReply: PropTypes.object,
  replyHandler: PropTypes.func,
  isSubReply: PropTypes.bool,
};

export default ReplyForm;
