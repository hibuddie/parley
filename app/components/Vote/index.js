import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import Humanize from 'humanize-plus';
import steem from 'steem';
import { ScaleLoader } from 'react-spinners';
import {getCurrentUser} from "utils/user";
import {STEEMIT_API_URL} from "containers/App/constants";
import './style.css';

class Vote extends React.PureComponent {

    constructor(props) {
        super(props);
        const {post} = props;
        const currentUser = getCurrentUser();

        let up_votable = true, down_votable = true;

        let filtered = [];
        if (currentUser) {
            if (post.active_votes)
                filtered = post.active_votes.filter(vote => vote.voter === currentUser.username);
            if (filtered.length > 0) {
                filtered[0].percent > 0 ? up_votable = false : down_votable = false;
            }
        } else {
            up_votable = false;
            down_votable = false;
        }

        this.state = {
            net_votes: props.post.net_votes,
            up_votable,
            down_votable,
            error: false,
        }
    }

    handler = (up) => {
        const {post} = this.props;
        const currentUser = getCurrentUser();
        const {username, password} = currentUser;
        const {author, permlink} = post;
        const {up_votable, down_votable, net_votes} = this.state;

        if (up > 0 && !up_votable) return;
        if (up < 0 && !down_votable) return;

        if (currentUser !== null) {
            steem.api.setOptions({ url: STEEMIT_API_URL });

            if (up === 1) { // upvote
                this.setState({
                    up_votable: false,
                    down_votable: true,
                    net_votes: net_votes + up});
                steem.broadcast.vote(password, username, author, permlink, 1000, (err, result)=>{
                    if (err) {
                        this.setState({error: err,
                            net_votes: net_votes + up,
                            up_votable: true});
                    }
                });
            } else { // downvote
                this.setState({
                    down_votable: false,
                    up_votable: true,
                    net_votes: net_votes + up});
                steem.broadcast.vote(password, username, author, permlink, -1000, (err, result) => {
                    if (err) {
                        this.setState({error: err,
                            net_votes: net_votes + up,
                            down_votable: true});
                    }
                });
            }
        }
    };

    render() {
        const currentUser = getCurrentUser();
        const {up_votable, down_votable} = this.state;
        const c = Humanize.compactInteger(this.state.net_votes, 1).toString();
        let activeUp = up_votable ? '' : 'fa-up-disabled';
        let activeDown = down_votable ? '' : 'fa-down-disabled';

        if (currentUser === null) {
            activeUp = 'fa-disabled';
            activeDown = 'fa-disabled';
        }
        return (
            <div className="item">
                <center>
                    <span onClick={() => this.handler(1)} className={`voteIcon ${activeUp}`}>
                        <FontAwesome name="chevron-circle-up" size="2x"/>
                    </span>
                    <div className="count">{ c }</div>
                    <span onClick={() => this.handler(-1)} className={`voteIcon ${activeDown}`}>
                        <FontAwesome name="chevron-circle-down" size="2x"/>
                    </span>
                </center>
            </div>
        );
    }
}

Vote.propTypes = {
    post: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.object
    ]),
};

export default Vote;
