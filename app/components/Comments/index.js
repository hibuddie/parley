import React from 'react';
import PropTypes from 'prop-types';
import ReplyForm from 'components/ReplyForm';
import CommentItem from 'components/CommentItem';
import './style.css';

export class Comments extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      reply: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.forceUpdate();
  }

  render() {
    const { post, replyHandler } = this.props;

    if (!post) {
      return false;
    }

    return (
      <div>
        <div id="comments-container" className="jquery-comments">
          <ReplyForm
            replyHandler={replyHandler}
            parentReply={post}
            isSubReply={false}
          />
          <div className="data-container" data-container="comments">
            <ul id="comment-list" className="main">
              { post.replies.map((reply) => (<CommentItem
                reply={reply}
                replyHandler={replyHandler}
                key={reply.id}
              />)).reverse()}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

Comments.propTypes = {
  post: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  replyHandler: PropTypes.func,
};

export default Comments;
