import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
    Nav,
    NavItem,
    NavLink,
    Button,
    Card,
    ButtonGroup,
    Col,
    Container,
    Row,
    Jumbotron } from 'reactstrap';
import InfiniteScroll from 'react-infinite-scroller';
import steem from 'steem';
import {STEEMIT_API_URL} from "containers/App/constants";
import './style.css';


class Tagbox extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            tags: false,
            loading: false,
            error: false,
        }
    }

    componentDidMount() {
        let currentComponent = this;
        steem.api.setOptions({ url: STEEMIT_API_URL });
        steem.api.getTrendingTags('', 1000, function(err, result) {
            const tags = result.map(tag => tag.name );
            currentComponent.setState({tags});
        });
    }

    render() {
        const {tags, hasMore} = this.state;
        if (!tags) {
            return null;
        }

        const content = tags.map(tag => (
                <li>
                    <a href={`/community/${tag}`}>{tag}</a>
                </li>
            ));

        return (
            <div>
                <h5>Communities</h5>
                <Card className="affix tagBox">

                    <ul className="tagList">
                        {content}
                    </ul>
                </Card>
            </div>

        )
    }
}

Tagbox.propTypes = {

};

export default Tagbox;