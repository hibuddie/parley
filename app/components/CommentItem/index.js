import React from 'react';
import PropTypes from 'prop-types';
import {
  Col,
  Row } from 'reactstrap';
import Markdown from 'markdown-to-jsx';
import Moment from 'react-moment';
import Vote from 'components/Vote';
import ReplyForm from 'components/ReplyForm';
import {getCurrentUser} from "utils/user";
import './style.css';

export class CommentItem extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      isViewAll: false,
      isEditShow: false,
      body: props.reply.body,
    };
  }
  componentWillReceiveProps(nextProps) {
    this.forceUpdate();
  }
  viewAll = () => {
    this.setState({
      isViewAll: !this.state.isViewAll,
    });
  };
  showEdit = () => {
    this.setState({isEditShow:true})
  };
  submitEdit = (event) => {
    event.preventDefault();
    const {reply, replyHandler} = this.props;
    const body = document.getElementById('edit-comment').value;

    const {parent_permlink, parent_author, permlink, author} = this.props.reply;
    const currentUser = getCurrentUser();
    const wif = currentUser.password;
    const payload = { body, parentPermlink: parent_permlink, parentAuthor: parent_author, author, permlink, wif };
    this.setState({isEditShow:false});
    replyHandler(payload);
  };
  changeHandler = () => {
    const body = document.getElementById('edit-comment').value;
    this.setState({body})
  };
    cancelEdit = () => {
        this.setState({isEditShow:false})
    };

  render() {
    const { reply, replyHandler } = this.props;
    const {isViewAll, isEditShow} = this.state;

    const currentUser = getCurrentUser();
    const { author, created, net_votes } = reply;
    const {body} = this.state;
    let isEditable = false;
    if (currentUser) {
        isEditable = reply.author === currentUser.username ? true : false;
    }

    let viewAllContent = '';
    let subreplies = reply.replies;
    if (isViewAll && reply && reply.replies && reply.replies.length > 0) {
      subreplies = reply.replies.slice(0, 2);
    }
    if (subreplies && subreplies.length > 2) {
      viewAllContent = <li className="toggle-all highlight-font-bold"><span className="text"><button onClick={this.viewAll}>View all {subreplies.length} replies</button></span><span className="caret" /></li>;
    }

    const bodyContent = isEditShow ?
        <form onSubmit={this.submitEdit}>
          <input type="text"
                  className="form-control"
                  defaultValue={body}
                  name="edit-commment"
                  id="edit-comment"
                 onChange={this.changeHandler}
                  autoFocus/>
            <div style={{textAlign: 'right'}}>
                <button className="btn-edit" style={{marginRight: '20px'}}>Send</button>
                <button className="btn-edit" onClick={this.cancelEdit}>Cancel</button>
            </div>
        </form> : <Markdown>{body}</Markdown>;

    return (
      <li className="comment">
        <div className="comment-wrapper">
          <Row>
            <Col xs="2" sm="1">
              <Vote post={reply} />
            </Col>
            <Col xs="10" sm="11">
              <Moment format="MM/DD/YYYY">{created}</Moment>
              <div className="name">{author} {isEditable && <span><button className="btn-edit" onClick={this.showEdit}>(Edit)</button></span>} </div>
              <div className="content">{bodyContent}</div>
              <ReplyForm
                replyHandler={replyHandler}
                parentReply={reply}
                isSubReply={true}
              />
              <ul>
                {viewAllContent}
                {subreplies && subreplies.map((r) =>
                  (<CommentItem
                    reply={r}
                    replyHandler={replyHandler}
                    key={r.id}
                  />)
                ).reverse()}
              </ul>
            </Col>
          </Row>
        </div>
      </li>
    );
  }

}

CommentItem.propTypes = {
    reply: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.object,
    ]),
    replyHandler: PropTypes.func,
};

export default CommentItem;
