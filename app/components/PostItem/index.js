import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import Vote from 'components/Vote';
import {
  Col, Row,
} from 'reactstrap';
import Humanize from 'humanize-plus';
import {calcPayout, removeParleyFromTag} from 'utils/commen'


class PostItem extends React.PureComponent {

    constructor(props) {
        super(props);
        this.onClick = this.handleClick.bind(this);
    }

    handleClick (event) {
        event.stopPropagation();
        if (event.target.tagName.toLowerCase() === 'a' || event.target.tagName.toLowerCase() === 'span' ||
            event.target.className === 'count' || event.target.className === 'item' || event.target.className.includes('vote'))
          return;

        const {post} = this.props;
        let tag = post.parent_permlink;
        if (tag === undefined) {
            tag = post.tags[0];
        }
        window.location.href = `/community/${removeParleyFromTag(tag)}`
    };

  render() {
      const {post} = this.props;
      const children = Humanize.formatNumber(post.children);
      const url = `${post.author}/${post.permlink}`;
      let tag = post.parent_permlink;
      if (tag === undefined) {
          tag = post.tags[0];
      }

      let videoUrl = '';
      if (post) {
          const jsonMetadata = JSON.parse(post.json_metadata);
          let isParley = false;
          if (jsonMetadata.app && jsonMetadata.app.name && jsonMetadata.app.name === 'parley') {
              isParley = true;
              videoUrl = jsonMetadata.appdata.parley.url;
          }
      }

      const payout = calcPayout(post);

      let titleLinkContent = <Link to={`/thread/${url}`}>{ post.title.toString() }</Link>;
      if (window.location.href.includes('/thread/')) {
          titleLinkContent = <a href={videoUrl}>{ post.title.toString() }</a>
      }

      return (
          <div>
            <Row onClick={this.onClick} className="postItem">
              <Col xs="2" sm="1" className="vote">
                <Vote post={post}/>
              </Col>
              <Col xs="10" sm="11">
                  <h4>
                      {titleLinkContent}
                  </h4>
                <p>
                  <span><FontAwesome name="money"></FontAwesome> { payout } </span>
                  <span><FontAwesome name="comments"></FontAwesome> { children } comments </span>
                  <span>by <Link to={`/user/${post.author}`}>{ post.author }</Link> in <a href={`/community/${removeParleyFromTag(tag)}`}>{ removeParleyFromTag(tag) }</a></span>
                </p>
              </Col>
            </Row>
            <br></br>
          </div>
      );
  }
}

PostItem.propTypes = {
    post: PropTypes.object,
};

export default PostItem;
